import sys
import os
import random
from time import strftime, sleep
from datetime import datetime, timezone
from traceback import format_exc as tracebackformat
import re

#                              Extra Modules
import discord  # pip install -U discord.py[voice]
import asyncio  # pip install -U discord.py[voice]
from discord.ext import commands  # pip install -U discord.py[voice]
import aiohttp

client = discord.Client()

@client.event
async def on_error(event, args="", kwargs=""):
    error = tracebackformat()
    print("BOT EXCEPTION:")
    print(error)
    if "KeyboardInterrupt" in error:
        print("KeyboardInterrupt")
        os._exit(1)
    try:
        with open("errors.txt", "a") as f:
            f.write("\n\n" + error + "\n\n")
    except:
        try:
            print("ERROR: NO ERRORS FILE, WRITING")
            with open("errors.txt", "w") as f:
                f.write("\n\n" + error + "\n\n")
        except:
            print("ERROR: NO FILESYSTEM ACCESS")


async def OverflowMessage(message, channel):
    try:
        cummsg = ""
        overflownewline = False
        for i in [x + "\n" for x in message.split("\n")]:
            if len(i) > 1900:
                overflownewline = True
                if cummsg != "":
                    await client.send_message(channel, cummsg)
                    message = message.replace(cummsg, "")
                break
            elif len(cummsg + i) > 1900:
                await client.send_message(channel, cummsg)
                message = message.replace(cummsg, "")
                cummsg = i
            else:
                cummsg += i
        if overflownewline:
            for i in message:
                if len(cummsg + i) > 1900:
                    await client.send_message(channel, cummsg)
                    message = message.replace(cummsg, "")
                    cummsg = i
                else:
                    cummsg += i
            if cummsg != "":
                await client.send_message(channel, cummsg)
                cummsg = ""
        else:
            await client.send_message(channel, cummsg)
        return True
    except Exception as e:
        return str(e)


async def CMD_forge(message):
    if message.content.startswith("/forge") and message.author.id == "278998128558866432":
        await client.delete_message(message)
        await client.send_message(message.channel, message.content.replace("/forge", "", 1))


async def CMD_oer(message):
    if message.content.startswith("/vowel") or message.content.startswith("/vwl"):
        vowel = "o"
        if len(message.content.split(" ")) >= 2:
            vowel = message.content.lower().split(" ")[1][0]
        async for msg in client.logs_from(message.channel, limit=50):
            if not (msg.content.startswith("/vowel")) and not (msg.content.startswith("/vwl")):
                newmsg = msg.content.replace("@everyone", "@ everyone").replace("@here", "@ here")
                vowelupper = vowel.upper()
                newmsg = newmsg.replace("a", vowel).replace("e", vowel).replace("i", vowel).replace("o", vowel).replace(
                    "u", vowel).replace("A", vowelupper).replace("E", vowelupper).replace("I", vowelupper).replace("O",
                                                                                                                   vowel).replace(
                    "U", vowelupper)

                await client.send_message(message.channel, newmsg)
                await client.delete_message(message)
                break


async def CMD_sarcasm(message):
    if message.content.startswith("/sarcasm") or message.content == ("/s"):
        msg2 = ""
        async for msg in client.logs_from(message.channel, limit=50):
            if not (msg.content.startswith("/s")):
                msg2 = msg.content
                break
        newmsg = msg2.split(" ")
        flip = False
        newsentence = []
        for word in newmsg:
            oldword = word
            newword = ""
            for letter in word:
                if flip:
                    newword += letter.upper()
                else:
                    newword += letter.lower()
                flip = not flip
            newsentence.append(newword)
        newsentence = ' '.join(newsentence)
        await client.send_message(message.channel, newsentence)
        await client.delete_message(message)


async def CMD_uwu(message):
    if message.content.startswith("/uwu"):
        async for msg in client.logs_from(message.channel, limit=50):
            if not (msg.content.startswith("/uwu")):
                newmsg = msg.content.replace("@everyone", "@ everyone").replace("@here", "@ here")
                newmsg = newmsg.replace("l", "w").replace("r", "w").replace("L", "W").replace("R", "W")
                newmsg = newmsg.replace("that", "dat").replace("That", "Dat").replace("THAT", "DAT")
                newmsg = newmsg.replace("the", "da").replace("The", "Da").replace("THE", "DA")
                newmsg = newmsg.replace("this", "dis").replace("This", "Dis").replace("THIS", "DIS")
                newmsg = newmsg.replace("y ", "ai ").replace("Y ", "AI ")
                newmsg = newmsg.replace("th ", "f ").replace("TH ", "F ")
                newmsg = newmsg + " " + random.choice(
                    ["*pounces on you* >w<", "*notices your buldge* OwO", "uwu", "daddy uwu", "uwu~~", "desu~",
                     "rawr x3", "x3", "wawr! xD", "murr~~", "<3", "nyeea~", "nya~", "o3o", "hehe ;)", "*nuzzles*"])

                await client.send_message(message.channel, newmsg)
                await client.delete_message(message)
                break


# async def CMD_blank(message):
#  if message.content.startswith("/blank"):
#    await client.send_message(message.channel,"** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **\n** **")
async def Keywords(message):
    keywords = ["Cold"]
    keywordslower = ["frozen"]
    keywordsno = ["maximum", "personalitymax"]
    if (not (any(x in message.content.lower() for x in keywordsno))) and (
            any(x in message.content for x in keywords) or any(x in message.content.lower() for x in keywordslower)):
        sugstr = "<@278998128558866432>\n__**Keyword Alert:**__\n{0}\n{1}\n```\n{2}```"
        username = "{0}#{1}".format(message.author.name,
                                    message.author.discriminator) if message.author.name == message.author.display_name else "{0} ({1}#{2})".format(
            message.author.display_name, message.author.name, message.author.discriminator)
        username = "**{0}** <{1}>:".format(username, message.author.id)
        content = message.content
        content = content.replace("@everyone", "@ everyone").replace("@here", "@ here")
        url = "https://discordapp.com/channels/{0}/{1}/{2}".format(message.server.id, message.channel.id, message.id)
        botchannel = bot.server.get_channel("584181762159280141")
        await client.send_message(botchannel, sugstr.format(url, username, content))

        if len(message.attachments) != 0:
            for i in message.attachments:
                try:
                    print(i["url"])
                    async with aiohttp.ClientSession() as cs:
                        async with cs.get(i["url"]) as r:
                            try:
                                content = await r.content.read()
                                with open(i["url"].split("/")[-1], "wb") as f:
                                    f.write(content)
                            except:
                                pass
                    tmp_sug = await client.send_file(botchannel, i["url"].split("/")[-1])
                    os.remove(i["url"].split("/")[-1])
                except:
                    print(tracebackformat())


@client.event
async def on_message(message):
    if not bot.ready:  # Race condition
        return
    if message.author == client.user or message.author.bot:
        return
    msg2 = message
    msg2.content = msg2.content.replace("@everyone", "@ everyone").replace("@here", "@ here")
    await CMD_forge(msg2)
    await Suggestions(msg2)
    await CMD_oer(msg2)
    await CMD_uwu(msg2)
    await CMD_sarcasm(msg2)
    await Keywords(msg2)


async def Suggestions(message):
    if message.channel == bot.channel_suggestions_human and (
            message.content.startswith(bot.suggestionschannel_prefix) or message.content.startswith("—")):
        message.content = message.content.replace(bot.suggestionschannel_prefix, "", 1)
        if message.content.startswith("<@") or message.content.startswith("<@!"):
            if message.content.startswith("<@!"):
                prefix = "<@!"
            else:
                prefix = "<@"
            authorid = message.content.split(prefix, 1)[1].split(">", 1)[0]
            mention = prefix + authorid + ">"
            suggestion = message.content.replace(mention, "").lstrip()
        else:
            suggestion = message.content
            try:
                name = message.author.id
                mention = "<@!" + name + ">"
            except Exception as e:
                print(str(e))
                name = "Unknown User"
                mention = "@" + name

        suggestion = suggestion.replace("@everyone", "@ everyone").replace("@here", "@ here")

        tmp = await client.send_message(bot.channel_suggestions_bot,
                            "** **\n** **\n__**SUGGESTION FROM " + mention + ":**__\n" + suggestion + "\n** **\n** **")
        if len(message.attachments) != 0:
            for i in message.attachments:
                try:
                    print(i["url"])
                    async with aiohttp.ClientSession() as cs:
                        async with cs.get(i["url"]) as r:
                            try:
                                content = await r.content.read()
                                with open(i["url"].split("/")[-1], "wb") as f:
                                    f.write(content)
                            except:
                                pass
                    tmp = await client.send_file(bot.channel_suggestions_bot, i["url"].split("/")[-1])
                    os.remove(i["url"].split("/")[-1])
                except:
                    print(tracebackformat())
        await client.add_reaction(tmp, '\N{THUMBS UP SIGN}')
        await client.add_reaction(tmp, '\N{THUMBS DOWN SIGN}')


async def dopin(reaction, user):
    global bot
    if reaction.message.id in bot.pinslist or reaction.message.channel == bot.channel_suggestions_bot or reaction.message.channel == bot.channel_suggestions_human:
        return
    counts = {}
    dopin = False
    for messagereaction in reaction.message.reactions:
        cur_reactionuserlist = await client.get_reaction_users(messagereaction, limit=99)
        if len(cur_reactionuserlist) >= 4:
            dopin = True
            break
    # if user.id == "278998128558866432":
    #  dopin = True
    if user.id == "204255221017214977":
        dopin = False
    if dopin:
        botchannel = bot.server.get_channel("617851712929267732")
        message = reaction.message
        sugstr = "__**{0}:**__\n{1}\n{2}"
        content = reaction.message.content
        content = content.replace("@everyone", "@ everyone").replace("@here", "@ here")
        tmp_sug = await client.send_message(botchannel, sugstr.format(
            message.author.display_name + " (" + message.author.name + "#" + message.author.discriminator + ")",
            "https://discordapp.com/channels/" + message.server.id + "/" + message.channel.id + "/" + message.id,
            reaction.message.content))
        bot.pinslist.append(message.id)
        try:
            with open("pins.txt", "w") as f:
                f.write(','.join(bot.pinslist))
        except:
            pass
        print("Suggestion ID:" + tmp_sug.id)
        if len(message.attachments) != 0:
            for i in message.attachments:
                try:
                    print(i["url"])
                    async with aiohttp.ClientSession() as cs:
                        async with cs.get(i["url"]) as r:
                            try:
                                content = await r.content.read()
                                with open(i["url"].split("/")[-1], "wb") as f:
                                    f.write(content)
                            except:
                                pass
                    tmp_sug = await  client.send_file(botchannel, i["url"].split("/")[-1])
                    os.remove(i["url"].split("/")[-1])
                except:
                    print(tracebackformat())


@client.event
async def on_reaction_add(reaction, user):
    if user == client.user:
        return
    await dopin(reaction, user)


@client.event
async def on_ready():
    global bot
    try:
        print('BOT_NAME: ' + client.user.name)
        print('BOT_ID: ' + client.user.id)
        bot.construction = False
        await client.change_presence( game=discord.Game(name="Loading..." if not bot.construction else "Under construction", type=0))

        bot.server = client.get_server("584173648513073177")
        bot.channel_suggestions_human = bot.server.get_channel("584504204417302537")
        bot.suggestionschannel_prefix = "--"
        bot.channel_suggestions_bot = bot.server.get_channel("617876934176669697")
        bot.functionlist = [i for i in dir(__import__(__name__)) if i.startswith("CMD_")]
        try:
            for channel in bot.server.channels:
                try:
                    if not (str(channel.type) == "text"):
                        continue
                    async for msg in client.logs_from(channel, limit=50):
                        client.messages.append(msg)
                except:
                    print("Failed Precache for: " + str(channel.name))
        except:
            print("PRECACHING ERROR:\n" + tracebackformat())

        try:
            with open("pins.txt", "r") as f:
                test = f.read()
            bot.pinslist = test.split(",")
        except:
            with open("pins.txt", "w") as f:
                f.write("")
            bot.pinslist = []

        await client.change_presence( game=discord.Game(name="LostType II" if not bot.construction else "Under construction", type=0))
        print("\n\n--READY--")
        bot.ready = True
    except:
        print("\n\n\nCRITICAL ERROR: FAILURE TO INITIALIZE")
        print(tracebackformat())
        client.close()
        client.logout()
        exit()
        raise Exception("CRITICAL ERROR: FAILURE TO INITIALIZE")
        return


class Varholder:
    pass


if __name__ == '__main__':
    try:
        global bot
        from sys import argv

        bot = Varholder()
        bot.ready = False
        bot.token = None
        print("--TOKEN_INIT--")
        try:
            bot.configname = argv[1]
            with open(bot.configname, "r") as f:
                bot.config = f.read()
            bot.token = bot.config.split("token=")[1].split("\n")[0]
            print("DISCORD TOKEN: {0}".format(
                bot.token[:int(len(bot.token) / 2)] + str("*" * (int(len(bot.token) / 2) - 4)) + bot.token[:4]))
            bot.config = [x for x in bot.config.split("\n") if x.strip()]
            bot.config.remove("token=" + bot.token)
            bot.client = client
        except:
            print("FAILURE TO READ CONFIG: INVALID TOKEN")
            print(tracebackformat())
            print("FAILURE TO READ CONFIG: INVALID TOKEN")
            raise Exception("FAILURE TO READ CONFIG: INVALID TOKEN")
        print("\n\n--LOGIN--")
        client.run(bot.token)
    except Exception as e:
        print("EXCEPTION \"{0}\"".format(tracebackformat()))
        try:
            client.close()
        except:
            pass
        try:
            client.logout()
        except:
            pass
        print("--FAILURE--")
        sleep(5)
